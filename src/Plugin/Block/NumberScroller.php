<?php

namespace Drupal\number_scroller\Plugin\Block;

use Drupal\node\Entity\NodeType;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "number_scroller_block",
 *   admin_label = @Translation("Number Scroller block"),
 *   category = @Translation("Number Scroller"),
 * )
 */
class NumberScroller extends BlockBase implements BlockPluginInterface {

  /**
   * Overrides \Drupal\block\BlockBase::blockForm().
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    global $base_url;
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('number_scroller')->getPath();

    // Fieldset.
    $form['numbers'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Numbers'),
    ];

    // Scroller starting value : Integer.
    $form['numbers']['start_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Scroller starting value'),
      '#description' => $this->t('Enter a starting number for scroller.'),
      '#default_value' => isset($config['start_value']) ? $config['start_value'] : 0,
      '#size' => 30,
    ];

    // Scroller starting value : Integer.
    $form['numbers']['end_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroller endinging value'),
      '#description' => $this->t('Enter a ending number for scroller.'),
      '#default_value' => isset($config['end_value']) ? $config['end_value'] : 1000,
      '#size' => 30,
    ];

    // Available tokens.
    $form['numbers']['token'] = [
      '#type' => 'details',
      '#title' => $this->t('Available tokens.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['numbers']['token']['token_info'] = [
      '#type' => 'table',
      '#header' => [$this->t('Name'), $this->t('Token'), $this->t('Description')],
      '#rows' => static::tokeninfo(),
    ];

    // Scroller increment value : Integer.
    $form['numbers']['increment'] = [
      '#type' => 'number',
      '#title' => $this->t('Scroller increment value'),
      '#description' => $this->t('Enter an increment value for each scrolling.'),
      '#default_value' => isset($config['increment']) ? $config['increment'] : 10,
      '#size' => 30,
    ];

    // Scroller delay value : Integer.
    $form['numbers']['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Scroller delay value'),
      '#description' => $this->t('Enter total seconds to complete the scrolling.'),
      '#default_value' => isset($config['delay']) ? $config['delay'] : 5,
      '#size' => 30,
    ];

    // Theme configurations fields.
    $theme_options = static::getThemeDropdownOptions();
    if ($config['active_theme']) {
      $active_theme = $config['active_theme'];
    }
    else {
      $active_theme = key($theme_options);
    }
    // Block theme fieldset - Fieldset.
    $form['block_theme'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Block theme'),
    ];
    $form['block_theme']['active_theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Select a theme for your block'),
      '#options' => $theme_options,
      '#default_value' => $active_theme,
    ];

    // Dark Theme.
    $form['block_theme']['dark_theme'] = [
      '#type' => 'container',
      '#placeholder' => 'Dark Theme',
      '#states' => [
        'visible' => [
          ':input[name="settings[block_theme][active_theme]"]' => ['value' => 'darktheme'],
        ],
      ],
    ];
    // Dark theme image.
    $imag_path = $base_url . '/' . $module_path . '/images/dark.png';
    $form['block_theme']['dark_theme']['dark'] = [
      '#type' => 'item',
      '#markup' => '<img src=' . $imag_path . ' />',
    ];

    // Light Theme.
    $form['block_theme']['light_theme'] = [
      '#type' => 'container',
      '#placeholder' => 'Light Theme',
      '#states' => [
        'visible' => [
          ':input[name="settings[block_theme][active_theme]"]' => ['value' => 'lighttheme'],
        ],
      ],
    ];
    // Light theme image.
    $imag_path = $base_url . '/' . $module_path . '/images/light.png';
    $form['block_theme']['light_theme']['light'] = [
      '#type' => 'item',
      '#markup' => '<img src=' . $imag_path . ' />',
    ];

    // Custom Theme.
    $form['block_theme']['custom_theme'] = [
      '#type' => 'container',
      '#placeholder' => 'Custom Theme',
      '#states' => [
        'visible' => [
          ':input[name="settings[block_theme][active_theme]"]' => ['value' => 'customtheme'],
        ],
      ],
    ];

    // Custom Theme Fields - Color field.
    $form['block_theme']['custom_theme']['background_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Background Color'),
      '#default_value' => isset($config['background_color']) ? $config['background_color'] : '#FFFFFF',
      '#description' => t('Background color for your block.'),
      '#size' => 30,
    ];

    // Custom Theme Fields - Font Size field - textfield.
    $form['block_theme']['custom_theme']['font_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Font size'),
      '#description' => $this->t('Font size of the scrolling number, allowed format : 14px, 2rem.'),
      '#default_value' => isset($config['font_size']) ? $config['font_size'] : '14px',
      '#size' => 30,
    ];

    // Custom Theme Fields - Font color field - color field.
    $form['block_theme']['custom_theme']['font_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Font Color'),
      '#default_value' => isset($config['font_color']) ? $config['font_color'] : '#000000',
      '#description' => t('Color of the font.'),
      '#size' => 30,
    ];

    // Custom Theme Fields - Text alignment field - select field.
    $alignement_options = static::getAlignementOptions();
    if ($config['alignement']) {
      $alignement_selected = $config['alignement'];
    }
    else {
      $alignement_selected = key($alignement_options);
    }
    $form['block_theme']['custom_theme']['alignement'] = [
      '#type' => 'select',
      '#title' => $this->t('Alignment of text in the block'),
      '#options' => $alignement_options,
      '#default_value' => $alignement_selected,
    ];

    // Custom Theme Fields - Spacing (padding) field - textfield.
    $form['block_theme']['custom_theme']['padding'] = [
      '#type' => 'textfield',
      '#title' => $this->t('spacing'),
      '#description' => $this->t('Space around the font (padding), allowed formats : px, % <br> Example : 50px 60px 50px 60px, 5% 10%'),
      '#default_value' => isset($config['padding']) ? $config['padding'] : '50px 60px 50px 60px',
      '#size' => 30,
    ];

    return $form;
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockSubmit().
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $this->configuration['start_value'] = $form_state->getValue(['numbers', 'start_value']);
    $this->configuration['end_value'] = $form_state->getValue(['numbers', 'end_value']);
    $this->configuration['increment'] = $form_state->getValue(['numbers', 'increment']);
    $this->configuration['delay'] = $form_state->getValue(['numbers', 'delay']);
    $this->configuration['active_theme'] = $form_state->getValue(['block_theme', 'active_theme']);
    $this->configuration['background_color'] = $form_state->getValue(['block_theme', 'custom_theme', 'background_color']);
    $this->configuration['font_size'] = $form_state->getValue(['block_theme', 'custom_theme', 'font_size']);
    $this->configuration['font_color'] = $form_state->getValue(['block_theme', 'custom_theme', 'font_color']);
    $this->configuration['alignement'] = $form_state->getValue(['block_theme', 'custom_theme', 'alignement']);
    $this->configuration['padding'] = $form_state->getValue(['block_theme', 'custom_theme', 'padding']);

  }

  /**
   * Overrides \Drupal\block\BlockBase::blockValidate().
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    $end_value_entered = $form_state->getValue(['numbers', 'end_value']);
    if (ctype_digit($end_value_entered) == FALSE) {
      // Consider the entered value is a token.
      $data = [];
      $end_token_value = \Drupal::token()->replace($end_value_entered, $data);
      if (ctype_digit($end_token_value) == FALSE) {
        // Set an error for the form element with a key of "title".
        $form_state->setErrorByName('end_value', $this->t('<b>Scroller endinging value</b> must be an integer. Integer valued tokens are also allowed'));
      }
    }
  }

  /**
   * Implements \Drupal\block\BlockBase::build().
   */
  public function build() {
    $config = $this->getConfiguration();

    if (strpos($config['end_value'], '[') !== FALSE) {
      // $page_count = "[number_scroller:page]";.
      $data = [];
      $end_token_value = \Drupal::token()->replace($config['end_value'], $data);
    }
    else {
      $end_token_value = $config['end_value'];
    }

    $start_value = $config['start_value'];
    $end_value = $end_token_value;
    $increment = $config['increment'];
    $delay = $config['delay'];
    $active_theme = $config['active_theme'];
    $background_color = $config['background_color'];
    $font_size = $config['font_size'];
    $font_color = $config['font_color'];
    $alignement = $config['alignement'];
    $padding = $config['padding'];

    return [
      '#start_value' => (int) $start_value,
      '#end_value' => (int) $end_value,
      '#increment' => (int) $increment,
      '#delay' => (int) $delay,
      '#active_theme' => $active_theme,
      '#background_color' => $background_color,
      '#font_size' => $font_size,
      '#font_color' => $font_color,
      '#alignement' => $alignement,
      '#padding' => $padding,
      '#theme' => 'numberscroller',
    ];
  }

  /**
   * GetThemeDropdownOptions.
   */
  public static function getThemeDropdownOptions() {
    return [
      'darktheme' => 'Dark Theme',
      'lighttheme' => 'Light Theme',
      'customtheme' => 'Custom Theme',
      'none' => 'None',
    ];
  }

  /**
   * GetAlignementOptions.
   */
  public static function getAlignementOptions() {
    return [
      'left' => 'Left',
      'center' => 'Center',
      'right' => 'Right',
    ];
  }

  /**
   * Tokeninfo.
   */
  public static function tokeninfo() {
    // Fetching nodes information.
    $node_types = NodeType::loadMultiple();
    foreach ($node_types as $node_type) {
      $rows[] = [
        'name' => $node_type->label(),
        'token' => '[number_scroller:' . $node_type->id() . ']',
        'description' => 'Count of nodes of type ' . $node_type->label(),
      ];
    }
    return $rows;
  }

}
