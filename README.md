# Contents Of This File

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## Introduction

Number scroller is a simple module that scrolls a number to a defined value.
The number scrolling starts once the block is visible in the page upon
scrolling. Below are the configurable parameters

 - Starting number
 - Ending number i.e. Integer tokens are accepted in this field, few sample tokens
   are created for reference
 - Increment
 - Delay or speed


## Requirements

This module requires no modules outside of Drupal core.


## INSTALLATION

 - Install the Number Scroller module as you would normally install a
   contributed Drupal module.
   Visit (https://www.drupal.org/node/1897420) for further information.


## Configuration

After module installation,
 - Navigate to `<yourdomain.com>/admin/structure/block`
 - Click on *place block* button in any region and place *Number Scroller block*
   block
 - Under block configurations enter relevant data

	**Numbers section**
	 - Scroller starting value
	 - Scroller ending value (accepts numeric tokens), few tokens are already
	   created
	 - Scroller increment value
	 - Scroller delay value

	**BLOCK THEME**
	Select a theme for the block
	 - Dark theme
	 - Light theme
	 - Custom theme i.e. has 5 fields - background color, font size, font color, text
	   alignment and spacing/padding
	 - None

 - Save block to see the number scroller


## Author/Maintainers

 - Ravi Kiran (ravi.kiran) - (https://www.drupal.org/u/ravikiran)
 - Sashi Kiran (sashi.kiran) - (https://www.drupal.org/u/sashikiran-0)
